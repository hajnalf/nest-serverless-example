import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CatsModule } from './cats/cats.module';
import { CatsService } from './cats/cats.service';

@Module({
  imports: [
    MongooseModule.forRoot(process.env.MONGODB_URI, { useNewUrlParser: true }),
    CatsModule
  ],
  controllers: [AppController],
  providers: [AppService, CatsService],
})
export class AppModule {}
