import { Controller, Get, Post, Body } from '@nestjs/common';
import { CatsService } from './cats.service';
import { Cat } from './interfaces/cat';
import { CreateCatDto } from './dto/create-cat.dto';

@Controller('cats')
export class CatsController {
    
  constructor(private readonly catsService: CatsService) {}

  @Get()
  async findAll(): Promise<Cat[]> {
    return this.catsService.findAll();
  }

  @Post()
  async create(@Body() createCatDto: CreateCatDto) : Promise<Cat> {
    return this.catsService.create(createCatDto);
  }
}
